<?php

namespace App\Admin;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;

class Admin extends DB
{

       public $fullName,$roll,$dob,$username,$email,$password,$address;


    public function setData($postData){

        //var_dump($postData);

         if(array_key_exists('fullName',$postData)){$this->fullName = $postData['fullName'];         }
         if(array_key_exists('roll',$postData)){$this->roll = $postData['roll']; }
         if(array_key_exists('dob',$postData)){$this->dob = $postData['dob']; }
         if(array_key_exists('username',$postData)){$this->username = $postData['username']; }
         if(array_key_exists('email',$postData)){$this->email = $postData['email']; }
         if(array_key_exists('password',$postData)){$this->password = $postData['password']; }
         if(array_key_exists('address',$postData)){$this->address = $postData['address']; }

     }


      public function store(){

          $arrData = array($this->fullName,$this->roll,$this->dob,$this->username,$this->email,$this->password,$this->address);

          //$sql = "INSERT into book_title(book_name,author_name) VALUES(?,?)";

          $sql="INSERT INTO user(name, roll,dob,username,email,password,address) VALUES(?,?,?,?,?,?,?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted :( ");

          Utility::redirect('index.php');


      }
   }