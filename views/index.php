<?php
include ('../vendor/autoload.php');

use App\Message\Message;
use App\Utility\Utility;


$objAdmin = new \App\Admin\Admin();

$msg = Message::getMessage();

//Utility::redirect('User/Profile/signup.php');

include ("header.php");
?>
<div class="text-center">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'> $msg</div> </div>"; ?>
</div>
<div align="center">
    <form action="store.php" method="post">
    <table>
       <tr><td>Name</td>  <td>:</td><td><input type="text" name="fullName" required></td></tr>
        <tr> <td>Roll</td>  <td>:</td><td><input type="number" name="roll" required></td></tr>
        <tr> <td>DOB</td>  <td>:</td><td><input type="date" name="dob" required></td></tr>
        <tr> <td>User</td>  <td>:</td><td><input type="text" name="username" required></td></tr>
        <tr> <td>Email</td>  <td>:</td><td><input type="email" name="email" required></td></tr>
        <tr> <td>Password</td>  <td>:</td><td><input type="password" name="password" required></td></tr>
        <tr> <td>Address</td>  <td>:</td><td><input type="text" name="address" required></td></tr>
        <tr> <td colspan="3" align="center"><input type="submit" value="Save"></td>
       </tr>

    </table>
    </form>
<a href="view.php">View Student</a>
</div>
</body>
</html>